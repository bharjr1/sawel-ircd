import sys
import socket
from threading import *

server_address = ('localhost', 6667)

class chatroom:
	def __init__(self,name):
		self.name = name
		self.users = { }
		self.userCount = 0
		self.userLeft = 0

	def addUser(self,connection):
		self.users[self.userCount] = connection
		self.userCount += 1

	def delUser(self,connection):
		self.users.remove(connection)

	def userList(self):
		out = str()
		for i in self.users:
			out = out + self.users[i].nick + " "
		return out

class conn:
	def __init__(self,ID,client_address,connection):
		global roomList
		self.ID = ID
		self.client_address = client_address
		self.connection = connection
		self.ident = str(self.ID) + "@" + self.client_address[0]
		self.t = Timer(0.0, self.recv)
		self.t.start()
		print self.ident + " connected"
		self.isOkay = True
		self.identTimer = Timer(0.0, self.getIdent)
		self.identTimer.start()

	def getIdent(self):
		while self.isOkay == True:
			try:
				self.nick and self.user
				self.connection.sendall(":" + server_address[0] + " 001 " + self.nick + " :Welcome\r\n")
				self.connection.sendall(":" + server_address[0] + " 002 " + self.nick + " :Your host is dreamhack.se.quakenet.org, running version u2.10.12.10+snircd(1.3.4a)\r\n")
				self.connection.sendall(":" + server_address[0] + " 003 " + self.nick + " :This server was created Fri Jun 22 2012 at 20:47:31 CEST\r\n")
				self.connection.sendall(":" + server_address[0] + " 004 " + self.nick + " :" + server_address[0] + " u2.10.12.10+snircd(1.3.4a) dioswkgxRXInP biklmnopstvrDcCNuMT bklov\r\n")
				self.connection.sendall(":" + server_address[0] + " 005 " + self.nick + " WHOX WALLCHOPS WALLVOICES USERIP CPRIVMSG CNOTICE SILENCE=15 MODES=6 MAXCHANNELS=20 MAXBANS=45 NICKLEN=15 :are supported by this server\r\n")
				self.connection.sendall(":" + server_address[0] + " 005 " + self.nick+ " MAXNICKLEN=15 TOPICLEN=250 AWAYLEN=160 KICKLEN=250 CHANNELLEN=200 MAXCHANNELLEN=200 CHANTYPES=& PREFIX=(ov)@+ STATUSMSG=@+ CHANMODES=b,k,l,imnpstrDducCNMT CASEMAPPING=rfc1459 NETWORK=QuakeNet :are supported by this server\r\n")
				self.connection.sendall(":" + server_address[0] + " 251 " + self.nick + " :There are 21 users and 43995 invisible on 35 servers\r\n")
				self.connection.sendall(":" + server_address[0] + " 251 " + self.nick + " 69 :operator(s) online\r\n")
				self.connection.sendall(":" + server_address[0] + " 251 " + self.nick + " 5 :unknown connection(s)\r\n")
				self.connection.sendall(":" + server_address[0] + " 251 " + self.nick + " 34019 :channels formed\r\n")
				self.connection.sendall(":" + server_address[0] + " NOTICE " + self.nick + " :Highest connection count: 6290 (6289 clients)\r\n")
				self.isOkay = False
				self.identTimer.stop()
				break
			except:
				continue
		self.ident = self.nick + "!~" + self.user + "@" + self.client_address[0]
		print "ID: " + str(self.ID) + ", ident sucesss (" + self.ident + ")"

	def recv(self):
		while True:
			try:
				self.data = self.connection.recv(256)
#				if self.data:
#					print "</RAW " + self.data + "/>"
				try:
					self.lista = self.data.split(":")
					self.extparam = self.lista[1]
					self.param = self.lista[0].split()
				except:
					self.param = self.data.split()

				if self.param[0] == "USER":
					self.user = self.param[1]
					self.real = self.extparam
					print self.ident + " set their username to " + self.user
					print self.ident + " set their real name to " + self.real
				elif self.param[0] == "NICK":
					self.nick = self.param[1]
					print self.ident + " set their nick to " + self.nick
				elif self.param[0] == "QUIT":
					print self.ident + ": disconnected"
					self.t.stop()
					self.identTimer.stop()
					self.connection.close()
					self.t.cancel()
					break
				elif self.param[0] == "ENDSERV":
#					sock.shutdown(sock.SHUT_RDWR)
					print self.ident + " ended the server."
					sys.exit()
					sock.close()
					return
					break
				elif self.param[0] == "JOIN":
					index,found = 0,False
					for i in roomList:
						if i.name == self.param[1]:
							found = True
							i.addUser(connec[self.ID])
							break
						else:
							index += 1
					if found == False:
						roomList.append(chatroom(self.param[1]))
						roomList[index].addUser(connec[self.ID])
					self.connection.sendall(":" + self.ident + " JOIN " + self.param[1] + "\r\n")
					self.connection.sendall(":" + server_address[0] + " 332 " + self.nick + " " + self.param[1] + " :MOTD\r\n")
					self.connection.sendall(":" + server_address[0] + " 333 " + self.nick + " " + self.param[1] + " :yano 1383013251\r\n")
					self.connection.sendall(":" + server_address[0] + " 353 " + self.param[1] + " :" + roomList[index].userList() + "\r\n")
					self.connection.sendall(":" + server_address[0] + " 366 " + self.nick + " " + self.param[1] + " :End of /NAMES list.\r\n")
					if found == True:
							for u in i.users:
								i.users[u].connection.sendall(":" + self.ident + " JOIN " + i.name + "\r\n")
					else:
							for u in roomList[index].users:
								roomList[index].users[u].connection.sendall(":" + self.ident + " JOIN " + roomList[index].name + "\r\n")
					print self.ident + " joined the channel " + roomList[index].name
				elif self.param[0] == "PRIVMSG":
					print self.ident + ">> " + self.extparam
					for room in roomList:
						if room.name == self.param[1]:
							for u in room.users:
								if not room.users[u] == connec[self.ID]:
									room.users[u].connection.sendall(":" + self.ident + " PRIVMSG " + room.name + " :" + self.extparam + "\r\n")
							break
#					senddata = str(self.ID) + ": " + self.extparam
#					for i in range(0,ID,1):
#						if i != self.ID:
#							":" + self.ident + " PRIVMSG " + [channel name?] + " :hello"
#							connec[i].connection.sendall(senddata)
			except:
				continue

	def close(self):
		self.connection.close()
		self.t.cancel()

roomList = [chatroom("java"), chatroom("daben")]

#connec[i].connection.sendall(message)

#create tcp/ip socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Bind the socket to ip:port and listen
print "Listening at " + server_address[0] + ":" + str(server_address[1])
sock.bind(server_address)
sock.listen(1)

connec,ID = { },0
while True:
#	try:
	connection, client_address = sock.accept()
	connec[ID] = conn(ID,client_address,connection)
	ID += 1
#	except:
#		sock.close()
#		print "Server closed!"
#		sys.exit()
#		break
print "lastline"
